import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dossier1Component } from './dossier1.component';

describe('Dossier1Component', () => {
  let component: Dossier1Component;
  let fixture: ComponentFixture<Dossier1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dossier1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dossier1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
