import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dossier1',
  templateUrl: './dossier1.component.html',
  styleUrls: ['./dossier1.component.css']
})
export class Dossier1Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  onBord(){
    this.router.navigate(['/dashbord']);

  }
  retour(){
    this.router.navigate(['/books']);
  }
  onMar(){
    this.router.navigate(['/dossier2']);
  }

}
