import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){
    var firebaseConfig = {
      apiKey: "AIzaSyBl5pxUxOV42IQuUWXfa-khhKBRITuobiI",
      authDomain: "je-suis-debutant.firebaseapp.com",
      databaseURL: "https://je-suis-debutant.firebaseio.com",
      projectId: "je-suis-debutant",
      storageBucket: "je-suis-debutant.appspot.com",
      messagingSenderId: "80062247028",
      appId: "1:80062247028:web:92996100f7656142646121",
      measurementId: "G-E8RQ39DTRB"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
}
}