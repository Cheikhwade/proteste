import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';

import { SingleBookComponent } from './book-list/single-book/single-book.component';
import { BookFormComponent } from './book-list/book-form/book-form.component';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule ,ReactiveFormsModule } from '@angular/forms';
import {RouterModule ,Routes } from '@angular/router';
import { AuthService } from './services/auth.service';
import { BooksService } from './services/books.service';
import { AuthGuardService } from './services/auth-guard.service';
import { DossierComponent } from './dossier/dossier.component';
import { BookListComponent } from './book-list/book-list.component';
import { Dossier1Component } from './dossier1/dossier1.component';
import { Dossier2Component } from './dossier2/dossier2.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import { DirigeComponent } from './dirige/dirige.component';
import { PrixComponent } from './prix/prix.component';
import { TestesComponent } from "./testes/testes.component";


const appRoutes: Routes = [
  {path: '', component: HeaderComponent},
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/signin', component: SigninComponent },
  { path: 'books', component: BookListComponent },
  { path: 'books/new', component: BookFormComponent },
  { path: 'books/view/:id', component: SingleBookComponent },
  {path: 'dossier', component: DossierComponent},
  {path: 'dossier1', component: Dossier1Component},
  {path: 'dossier2', component: Dossier2Component},
  {path: 'dashbord', component: DashbordComponent},
  {path: 'dirige', component: DirigeComponent},
  {path: 'prix', component: PrixComponent},
  {path: 'testes', component: TestesComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    BookListComponent,
    SingleBookComponent,
    BookFormComponent,
    HeaderComponent,
    DossierComponent,
    Dossier1Component,
    Dossier2Component,
    DashbordComponent,
    DirigeComponent,
    PrixComponent,
    TestesComponent
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    BooksService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
