import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Dossier2Component } from './dossier2.component';


describe('Dossier2Component', () => {
  let component: Dossier2Component;
  let fixture: ComponentFixture<Dossier2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dossier2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dossier2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
