import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import { from } from 'rxjs';

@Component({
  selector: 'app-prix',
  templateUrl: './prix.component.html',
  styleUrls: ['./prix.component.css']
})
export class PrixComponent implements OnInit {
  loginForm: FormGroup;
  prix: any;

  constructor(private router: Router , private fb: FormBuilder) { }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      prix: [],
    })
  }
  login(){
    this.loginForm.get('prix').value;
    this.router.navigate(['/dossier2']);
  }
 
} 